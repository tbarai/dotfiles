# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

#### HISTORY 
#
# HISTIGNORE="ls:ll:pwd:clear:top"
#
# don't put duplicate lines or lines starting with space in the history.
# You can concatenate and separate the values with a colon, ignorespace:ignoredups
HISTCONTROL=ignoreboth
#
# append to the history file, don't overwrite it
shopt -s histappend
#
# If HISTFILESIZE is not set, no truncation is performed
# "the same seems to apply to HISTSIZE, although I couldnt find that documented"
# https://superuser.com/questions/137438/how-to-unlimited-bash-shell-history
HISTSIZE=
HISTFILESIZE=
#
# Change the file location because certain bash sessions truncate .bash_history file upon close.
# http://superuser.com/questions/575479/bash-history-truncated-to-500-lines-on-each-login
# export HISTFILE=~/.bash_eternal_history
#
## Force prompt to write history after every command.
## http://superuser.com/questions/20900/bash-history-loss
## -a	Append the history lines entered since the beginning of the current Bash session to the history file
## -c	Clear the history list
## -r	Read the history file and append its contents to the history list
#PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"
#shopt -s histappend
## (?)
#stophistory () {
#  PROMPT_COMMAND="bash_prompt_command"
#  echo 'History recording stopped.'
#}
#
# HISTTIMEFORMAT="%Y%m%d %T "
# http://unix.stackexchange.com/questions/97983/histtimeformat-modification-in-etc-bash-rc-or-etc-profile
# HISTTIMEFORMAT="%F %T "
#
#### /HISTORY


# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi


#### COLORS
#
#
##### spostato (parzialmente) in ~/my-local
# # uncomment for a colored prompt, if the terminal has the capability; turned
# # off by default to not distract the user: the focus in a terminal window
# # should be on the output of commands, not on the prompt
# force_color_prompt=yes
# 
# if [ -n "$force_color_prompt" ]; then
#     if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
# 	# We have color support; assume it's compliant with Ecma-48
# 	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
# 	# a case would tend to support setf rather than setaf.)
# 	color_prompt=yes
#     else
# 	color_prompt=
#     fi
# fi
# 
# if [ "$color_prompt" = yes ]; then
#     PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
# else
#     PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
# fi
# unset color_prompt force_color_prompt
#####
#
#
# enable color support of ls 
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# https://wiki.archlinux.org/index.php/Man_page#Colored_man_pages
#
man() {
    env \
    LESS_TERMCAP_mb=$'\e[01;31m' \
    LESS_TERMCAP_md=$'\e[01;31m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_se=$'\e[0m' \
    LESS_TERMCAP_so=$'\e[01;44;33m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[01;32m' \
    man "$@"
}

#
#### /COLORS


if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi



# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then PATH="$HOME/bin:$PATH" ; fi




# cose da fare subito prima di mostrare il prompt

# keychain
# implica richiesta delle passphrases
if [ -f ~/.keychain-my ] ; then source ~/.keychain-my ; fi

# z
if [ -f ~/bin/z.sh ] ; then source ~/bin/z.sh ; fi

# cosa mettiamo in ~/my-local ?
# colori, variabili d'ambiente
if [ -f ~/my-local ] ; then source ~/my-local ; fi

# ~/bin/todo è lo script, ~/todo è la lista scritta dallo script
if [ -f ~/todo ] ; then ~/bin/todo ; fi


# https://unix.stackexchange.com/questions/4859/visual-vs-editor-whats-the-difference
if [ -x /usr/bin/vim ] ; then
export EDITOR='/usr/bin/vim'
export VISUAL=/usr/bin/vim
fi

#export GPG_TTY=$(tty)

